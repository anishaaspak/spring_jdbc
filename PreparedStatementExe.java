package com.example.Spring_Jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementExe {


    public static void runStmt(Connection con, int data)
    {
        try {
            PreparedStatement ps = con.prepareStatement("insert into student values(?,?,?)");
            ps.setInt(1,1);
            ps.setString(2,"Ratan");
            ps.setInt(3, 21);

            ps.execute();
            System.out.println("inserted data");
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

}
