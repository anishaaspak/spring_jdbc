package com.example.Spring_Jdbc;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.SQLException;

public class RowSetExe {
    public static void runStmt() {
        try{
            JdbcRowSet rs = RowSetProvider.newFactory().createJdbcRowSet();
            rs.setUrl("jdbc:mysql://localhost:8086/test");
            rs.setUsername("root");
            rs.setPassword("Anis99*");
            rs.setCommand("select * from student");
            rs.execute();
            while(rs.next())
            {
                System.out.print("Id: " + rs.getInt(2));
                System.out.println("Name: " + rs.getString(2));
                System.out.println("Age: " + rs.getInt(3));
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }

    }
}
