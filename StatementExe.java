package com.example.Spring_Jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementExe {
    public static void runStmt(Connection con) {
        try
        {
            Statement stmt = con.createStatement();
            ResultSet fetchData = stmt.executeQuery("select * from student");
            while(fetchData.next())
            {
                System.out.print("Id: " + fetchData.getInt(2));
                System.out.println("Name: " + fetchData.getString(2));
                System.out.println("Age: " + fetchData.getInt(3));
            }
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }

    }
}
