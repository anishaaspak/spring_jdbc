package com.example.Spring_Jdbc;
import java.sql.*;
import java.util.Scanner;


public class MysqlCon {
    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);
        int value;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:8086/test","root","Anis99*");
            System.out.println("Enter a value to insert : ");
            value = scan.nextInt();
            PreparedStatementExe.runStmt(con,value);
            StatementExe.runStmt(con);
            con.close();
            System.out.println();
            RowSetExe.runStmt();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}


